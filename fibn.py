import argparse

def fib(n): # fibonacci function
    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    return a

def Main(): # Main function
    parser = argparse.ArgumentParser() #argparser init instace
    group = parser.add_mutually_exclusive_group() # group for adding arguments
    group.add_argument("-v","--verbose", action="store_true")
    group.add_argument("-q","--quiet", action="store_true")
    parser.add_argument("num", help="The fibonacci number " + \
                        "you wish to calculate.", type=int) # adding args
    parser.add_argument("-o","--output", help="Output the " + \
                        "result to a file", action="store_true") # arg + output to a file
                        
    args = parser.parse_args() #storing parser.args to args

    result = fib(args.num) # store on result
    if args.verbose:
        print (" the " +str(args.num)+ " th fib number is " +str(result)) # print output
    elif args.quiet:
        print(result)
    else:
        print ("Fib("+str(args.num)+") = " +str(result))

    if args.output: # check if args,output exist
        f = open("fibonacci.txt", "a") # open fibonacci.txt
        f.write(str(result) + '\n') # write on fibonacci.txt


if __name__=='__main__': 
    Main() # calling main function
                        
    
